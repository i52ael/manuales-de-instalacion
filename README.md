# Manuales de Instalacion


***--------DEBIAN 9---------***

***NVM***

1. sudo apt-get install curl
2. curl -o- ``https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh`` | bash
3. COPIAR EL TEXTO DE ABAJO TAL CUAL Y PEGAR EN TERMINAL Y DAR ENTER

    export NVM_DIR="$HOME/.nvm"

4. COPIAR EL TEXTO DE ABAJO TAL CUAL Y PEGAR EN TERMINAL Y DAR ENTER

    [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  


***GIT***
1. apt-get install git


***MONGO DB opcion 1***

1. apt-get install mongodb

***MONGO DB opcion 2***
**pendiente**
1. instalar dirmngr

sudo apt-get install dirmngr

2. ejecutar la instruccion

sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 9DA31620334BD75D9DCB49F368818C72E52529D4

3. crear el archivo mongodb-org-4.0.list
sudo nano  /etc/apt/sources.list.d/mongodb-org-4.0.list

***robo3t (administrador de mongodb)***

1. descargar la version Robo 3T

2. desenpaquetar el archivo en /usr/local/

    sudo tar -zxvf robo3t-1.2.1-linux-x86_64-3e50a65.tar.gz -C /usr/local/
3. ir a la ruta ``/usr/local/``
    
    cd /usr/local

4. cambiar el nombre de la carpeta

    sudo mv robo3t-1.2.1-linux-x86_64-3e50a65 robo3tgit@gitlab.geo.gob.bo:agetic/iop-servicios-unidad-discapacidad-v1.git

5. entrar a la carpeta bin

    cd robo3t/bin
    
6. ejecutar el archivo ``robo3t``

    ./robo3t 


***JAVA JDK***



1. Verificar la version actual
    java -version

2. Verificar si existe la carpeta ``jvm`` sino existe crear  

    sudo mkdir -p ``/usr/lib/jvm``
    
3. Descargar el archivo ``jdk-8u65-linux-x64.tar.gz``, en google buscar java jdk la ultima version

4. Descomprimir

    sudo tar -zxvf jdk-8u65-linux-x64.tar.gz -C /usr/lib/jvm
    
5. Ejecutar la siguiente linea (donde dice version cambiar por la que descargo)

    sudo update-alternatives --install "/usr/bin/java" "java" "/usr/lib/jvm/jdk1.8.0_``version``/bin/java" 1

6. Ejecutar la siguiente linea (si existe otra version nos mostrara un menu para setear la version que deseamos utilizar)

    sudo update-alternatives --config java
    
7. Verificar la version    
    
    java -version

***--------GIT llaves ssh---------***

1. verificar que la carpeta ``.ssh`` exista
2. si no existe la carpeta hay que crearla

    mkdir .ssh
    
    chown usuario:grupo .ssh
    
3. entrar a la carpeta
    cd .ssh

4.generar la llave

 ssh-keygen -t rsa -b 4096 -C "israel.limachi@produccion.gob.bo"

4.1 nos indicara su queremos cambiar el nombre de la llave (si no introducimos nada se crear con ``id_rsa``) damos enter

Enter file in which to save the key (/home/israel/.ssh/id_rsa):

4.2 nos pedira que introduzacamos un palabra clave para encriptar y damos enter

Enter passphrase (empty for no passphrase):

5. ver el contenido del archivo ``id_rsa.pub`` (el archivo con la extencion ``.pub``)
    
    cat id_rsa.pub

6. copiar la llave al servidor (en este paso pide introducir una contraseña pero no se cual es)

    ssh-copy-id -i ~/.ssh/id_rsa.pub israel.limachi@geo.gob.bo
    
7. ir a gitlab o github (funciona para los dos) a settings -> SSH keys

8. copiar todo lo que contiene la llave en key
    
ssh-rsa AAAAB3NzaC1y...............
...........b7m8g7/B11Q== israel.limachi@produccion.gob.bo

9. i clonar



***MONGO***

1. restaurar backup
    
    mongorestore --drop -d <database-name> <directory-of-dumped-backup>

Ejemplo mongorestore --drop -d unidad-discapacidad-v1 backup-10-10-2018/



***--------LUMEN 5.7---------***
**CONEXION A MAS DE UNA BASE DE DATOS**
1. Create folder config 
2. Create file config/database.php 

    app 
    bootstrap   
    config  
            |--``databse.php``   
    database    
    public  
    resources   
    .   
    .   
    .   
    vendor  
    

3. Contents of the file database.php

``<?php``

return [

    "fetch" => PDO::FETCH_CLASS,
    "default" => "pgsql", 
    "connections" => [
    
        "pgsql" => [
          "driver"   => "pgsql",
          "host"     => env('DB_HOST', '192.168.0.13'),
          "database" => env('DB_DATABASE', 'probolivia'),
          "username" => env('DB_USERNAME', 'postgres'),
          "password" => env('DB_PASSWORD', 'postgres'),
          "charset"  => 'utf8',
          "prefix"   => "",
          "schema"   => "public",
        ],
        "pgsql2" => [
          "driver"   => 'pgsql',
          "host"     => env('DB2_HOST', '192.168.0.14'),
          "database" => env('DB2_DATABASE', 'doble_prueba'),
          "username" => env('DB2_USERNAME', 'postgres'),
          "password" => env('DB2_PASSWORD', 'null'),
          "charset"  => 'utf8',
          "prefix"   => "",
          "schema"   => "public",
        ],

    ],
    
    "migrations" => "migrations",
];

4. Modify file bootstrap/app.php

  $app->withFacades(); // descomentar
  
  $app->configure('database'); // adicionar
  
  $app->withEloquent(); // descomentar
  

**ADICIONANDO JWT**

1. En el archivo .env adicionar 

    APP_KEY=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9
    JWT_SECRET=JhbGciOiJIUzI1N0eXAiOiJKV1QiLC   
    
2. Crear el archivo app/Http/Controllers/AuthController.php


``<?php``

    namespace App\Http\Controllers;
    use Validator;
    use App\User;
    use Firebase\JWT\JWT;
    use Illuminate\Http\Request;
    use Firebase\JWT\ExpiredException;
    use Illuminate\Support\Facades\Hash;
    use Laravel\Lumen\Routing\Controller as BaseController;
    class AuthController extends BaseController
    {
        /**
         * The request instance.
         *
         * @var \Illuminate\Http\Request
         */
        private $request;
        /**
         * Create a new controller instance.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return void
         */
        public function __construct(Request $request) {
            $this->request = $request;
        }
        /**
         * Create a new token.
         *
         * @param  \App\User   $user
         * @return string
         */
        protected function jwt(User $user) {
            $payload = [
                'iss' => "lumen-jwt", // Issuer of the token
                'sub' => $user->id, // Subject of the token
                'iat' => time(), // Time when JWT was issued.
                'exp' => time() + 60*60 // Expiration time
            ];
    
            // As you can see we are passing `JWT_SECRET` as the second parameter that will
            // be used to decode the token in the future.
            return JWT::encode($payload, env('JWT_SECRET'));
        }
        /**
         * Authenticate a user and return the token if the provided credentials are correct.
         *
         * @param  \App\User   $user
         * @return mixed
         */
        public function authenticate(User $user) {
            $this->validate($this->request, [
                'email'     => 'required|email',
                'password'  => 'required'
            ]);
            // Find the user by email
            $user = User::where('email', $this->request->input('email'))->first();
            if (!$user) {
                // You wil probably have some sort of helpers or whatever
                // to make sure that you have the same response format for
                // differents kind of responses. But let's return the
                // below respose for now.
                return response()->json([
                    'error' => 'Email does not exist.'
                ], 400);
            }
            // Verify the password and generate the token
            if (Hash::check($this->request->input('password'), $user->password)) {
                return response()->json([
                    'token' => $this->jwt($user)
                ], 200);
            }
            // Bad Request response
            return response()->json([
                'error' => 'Email or password is wrong.'
            ], 400);
        }
    }
    

3. adicionar el archivo app/Http/Middleware/JwtMiddleware.php

    ``<?php``   
    namespace App\Http\Middleware;  
    use Closure;    
    use Exception;  
    use App\User;   
    use Firebase\JWT\JWT;   
    use Firebase\JWT\ExpiredException;  
    class JwtMiddleware 
    {
        public function handle($request, Closure $next, $guard = null)
        {
            $token = $request->get('token');
        
            if(!$token) {
                // Unauthorized response if token not there
                return response()->json([
                    'error' => 'Token not provided.'
                ], 401);
            }
            try {
                $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);
            } catch(ExpiredException $e) {
                return response()->json([
                    'error' => 'Provided token is expired.'
                ], 400);
            } catch(Exception $e) {
                return response()->json([
                    'error' => 'An error while decoding token.'
                ], 400);
            }
            $user = User::find($credentials->sub);
            // Now let's put the user in the request class so that you can grab it from there
            $request->auth = $user;
            return $next($request);
        }
    }

 
4. adifionar el middleware en el archivo bootstrap/app.php

    $app->routeMiddleware([
        'jwt.auth' => App\Http\Middleware\JwtMiddleware::class,
    ]);

5. 